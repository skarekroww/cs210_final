#include <Python.h>
#include <iostream>
#include <Windows.h>
#include <cmath>
#include <string>
#include <fstream>
#include <cstring>

/*
Description:
	To call this function, simply pass the function name in Python that you wish to call.
Example:
	callProcedure("printsomething");
Output:
	Python will print on the screen: Hello from python!
Return:
	None
*/
using namespace std;
void CallProcedure(string pName)
{
	char* procname = new char[pName.length() + 1];
	std::strcpy(procname, pName.c_str());

	Py_Initialize();
	PyObject* my_module = PyImport_ImportModule("CS210_Starter_PY_Code.py");
	PyErr_Print();
	PyObject* my_function = PyObject_GetAttrString(my_module, procname);
	PyObject* my_result = PyObject_CallObject(my_function, NULL);
	Py_Finalize();

	delete[] procname;
}

/*
Description:
	To call this function, pass the name of the Python functino you wish to call and the string parameter you want to send
Example:
	int x = callIntFunc("PrintMe","Test");
Output:
	Python will print on the screen:
		You sent me: Test
Return:
	100 is returned to the C++
*/
int callIntFunc(string proc, string param)
{
	char* procname = new char[proc.length() + 1];
	std::strcpy(procname, proc.c_str());

	char* paramval = new char[param.length() + 1];
	std::strcpy(paramval, param.c_str());


	PyObject* pName, * pModule, * pDict, * pFunc, * pValue = nullptr, * presult = nullptr;
	// Initialize the Python Interpreter
	Py_Initialize();
	// Build the name object
	pName = PyUnicode_FromString((char*)"CS210_Starter_PY_Code.py");
	// Load the module object
	pModule = PyImport_Import(pName);
	// pDict is a borrowed reference
	pDict = PyModule_GetDict(pModule);
	// pFunc is also a borrowed reference
	pFunc = PyDict_GetItemString(pDict, procname);
	if (PyCallable_Check(pFunc))
	{
		pValue = Py_BuildValue("(z)", paramval);
		PyErr_Print();
		presult = PyObject_CallObject(pFunc, pValue);
		PyErr_Print();
	}
	else
	{
		PyErr_Print();
	}
	//printf("Result is %d\n", _PyLong_AsInt(presult));
	Py_DECREF(pValue);
	// Clean up
	Py_DECREF(pModule);
	Py_DECREF(pName);
	// Finish the Python Interpreter
	Py_Finalize();

	// clean
	delete[] procname;
	delete[] paramval;


	return _PyLong_AsInt(presult);
}

/*
Description:
	To call this function, pass the name of the Python functino you wish to call and the string parameter you want to send
Example:
	int x = callIntFunc("doublevalue",5);
Return:
	25 is returned to the C++
*/
int callIntFunc(string proc, int param)
{
	char* procname = new char[proc.length() + 1];
	std::strcpy(procname, proc.c_str());

	PyObject* pName, * pModule, * pDict, * pFunc, * pValue = nullptr, * presult = nullptr;
	// Initialize the Python Interpreter
	Py_Initialize();
	// Build the name object
	pName = PyUnicode_FromString((char*)"CS210_Starter_PY_Code.py");
	// Load the module object
	pModule = PyImport_Import(pName);
	// pDict is a borrowed reference
	pDict = PyModule_GetDict(pModule);
	// pFunc is also a borrowed reference
	pFunc = PyDict_GetItemString(pDict, procname);
	if (PyCallable_Check(pFunc))
	{
		pValue = Py_BuildValue("(i)", param);
		PyErr_Print();
		presult = PyObject_CallObject(pFunc, pValue);
		PyErr_Print();
	}
	else
	{
		PyErr_Print();
	}
	//printf("Result is %d\n", _PyLong_AsInt(presult));
	Py_DECREF(pValue);
	// Clean up
	Py_DECREF(pModule);
	Py_DECREF(pName);
	// Finish the Python Interpreter
	Py_Finalize();

	// clean
	delete[] procname;

	return _PyLong_AsInt(presult);
}


int main()
{
	int input = 0;
	string searchTerm;
	string itemName;
	int itemCount;
	ifstream fileInput;
	// We want this loop to run till exit num is given
	while (true) {
		system("cls");
		cout << "Please enter a valid number 1-4" << endl;
		cout << "1: Count the number of times each item appears in the text" << endl;
		cout << "2: Find the count of a specific item within the text file." << endl;
		cout << "3: Create a histogram based off the counts of each item." << endl;
		cout << "4: Quit out of this application." << endl;

        cin >> input; // user input for the switch

		switch (input) {
		case 1: system("cls"); CallProcedure("ItemCounter"); break; // runs ItemCounter. Prints the info within the function itself. Nothing needs to be passed.
		case 2:
			system("cls");
			cout << "What are you searching for?" << endl;
			cin >> searchTerm; //user input item that is hopefully within text file
			cout << searchTerm << " came up " << callIntFunc("SpecificCounter", searchTerm) << " times."  << endl;    //calls function to search for whatever word is passed
			break;
		case 3:
			system("cls");
			CallProcedure("ItemCounter"); //Assuming 1 wasn't ran
			fileInput.open("frequency.dat"); //opens out file created in the ItemCounter function
            fileInput >> itemName ;
            fileInput >> itemCount;

			while (!fileInput.fail()) { //loop to go through the file

				cout << itemName << "\t"; // prints out item Name and tabs over
				for (int x = 0; x < itemCount; x++) { // loops the itemCount and prints a * each time
					cout << "*";
				}
				cout << endl;

                fileInput >> itemName ;
                fileInput >> itemCount;
			}
            fileInput.close();
			break;
		case 4: exit(0);  break; //Reports successful termination of the program.
		}

	}

}