cmake_minimum_required(VERSION 3.22)
project(Project3_Final_Week7)

set(CMAKE_CXX_STANDARD 14)

add_executable(Project3_Final_Week7 CS210_Starter_PY_Code.py main.cpp)

find_package(PythonLibs REQUIRED)
include_directories(${PYTHON_INCLUDE_DIRS})
target_link_libraries(Project3_Final_Week7 ${PYTHON_LIBRARIES})