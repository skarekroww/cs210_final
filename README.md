**What is this application?**<br>
This application was the final projcet for CS210- Programming Languages. Within is a .cpp file and a .py file. The goal of this file was to get the .cpp file to call functions from the .py file. There are 3 main functions. _First_ to generate a frequancy.dat file using the information from CS210_Project_Three_Input_File.txt. _Second_ Find the amount of times a specific item in the .txt file comes up. _Third_ create a historgram using the information within the frequency.dat file.
<br>

**How to build**<br>
You have to set up your IDE to run both python and C++. Personally I fialed this during this project and had a shot in the dark for the final script. This respository has a CMAKE file that improperly added CS210_Starter_PY_Code.py as a module and didn't allow the main.cpp to run it.<br>

**Used Languages**<br>
Python and C++<br>

**Known Issues / What could be better**<br>
The historgram doesn't function properly as well as the CMAKE file isn't properly setup. The fact the CMAKE file isn't properly set up causes issues with the languages interacting together properly. Issue causes that can be fixed is "ModuleNotFoundError: No module named 'CS210_Starter_PY_Code'". 
