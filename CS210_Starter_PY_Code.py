import re
import string
import collections


# These 3 functions were here with the starter file
## They're not needed but since they were here i'll keep them
def printsomething():
    print("Hello from python!")


def PrintMe(v):
    print("You sent me: " + v)
    return 100;


def SquareValue(v):
    return v * v


# Assuming input file is where the python file is ./
def ItemCounter():
    with open('CS210_Project_Three_Input_File.txt') as file:
        # collections.Counter does what we need automatically. They count the times and item appears.
        itemCounts = collections.Counter(line.strip() for line in file)
    # This should cycle through and print everything out
    for key in itemCounts:
        print('%s %d' % (key, itemCounts[key]))


def SpecificCounter(itemName):
    print("python was called here")
    # capitalizes the first letter in case it wasn't already
    itemName = itemName.capitalize()
    with open('CS210_Project_Three_Input_File.txt') as file:
        item = file.read()
        itemCount = item.count(itemName)
    return itemCount


def ItemHistogram():
    # Open file to write to it
    with open('frequency.dat', "w") as writeFile:
        # Same code as in "ItemCounter" to store the values as a dictionary
        with open('CS210_Project_Three_Input_File.txt') as itemCountFile:
            counts = collections.Counter(line.strip() for line in itemCountFile)
        # Write the item and counts to the output file each item gets a new(\n) line
        for key in counts:
            writeFile.write('%s %d\n' % (key, counts[key]))
    # This was for debug purposes should let me know if file was made
    if writeFile.closed:
        print('Histogram Made')


# These were called so i know each script worked and to show they work
print("")
print("Specifically Spinach ", SpecificCounter("Spinach"))
print("")
ItemCounter()

ItemHistogram()

